package com.ticxo.demomodel.model.animation.attack;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.EulerAngle;

import com.ticxo.modelapi.api.animation.Animation;
import com.ticxo.modelapi.api.animation.preset.KeyFrame;
import com.ticxo.modelapi.api.animation.preset.Sequence;
import com.ticxo.modelapi.api.animation.preset.SequenceAnimation;
import com.ticxo.modelapi.api.modeling.Part;

public class AnimationCast implements Animation{

	private Map<String, Animation> animation = new HashMap<String, Animation>();
	
	public AnimationCast() {
		
		List<KeyFrame> rightArmKeys = new ArrayList<KeyFrame>();
		rightArmKeys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-20), Math.toRadians(-65), Math.toRadians(30))));
		rightArmKeys.add(new KeyFrame(10, new EulerAngle(Math.toRadians(-30), Math.toRadians(-25), Math.toRadians(20))));
		rightArmKeys.add(new KeyFrame(35, new EulerAngle(Math.toRadians(-30), Math.toRadians(-25), Math.toRadians(20))));
		rightArmKeys.add(new KeyFrame(40, new EulerAngle(0, 0, Math.toRadians(20))));
		rightArmKeys.add(new KeyFrame(46, new EulerAngle(0, 0, Math.toRadians(20))));
		rightArmKeys.add(new KeyFrame(50, new EulerAngle(Math.toRadians(-20), Math.toRadians(-65), Math.toRadians(30))));
		animation.put("phantom_empress/rightarm", new SequenceAnimation(new Sequence(rightArmKeys)));
		
		List<KeyFrame> leftArmKeys = new ArrayList<KeyFrame>();
		leftArmKeys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-20), Math.toRadians(65), Math.toRadians(-30))));
		leftArmKeys.add(new KeyFrame(10, new EulerAngle(Math.toRadians(-30), Math.toRadians(25), Math.toRadians(-20))));
		leftArmKeys.add(new KeyFrame(35, new EulerAngle(Math.toRadians(-30), Math.toRadians(25), Math.toRadians(-20))));
		leftArmKeys.add(new KeyFrame(40, new EulerAngle(0, 0, Math.toRadians(-20))));
		leftArmKeys.add(new KeyFrame(46, new EulerAngle(0, 0, Math.toRadians(-20))));
		leftArmKeys.add(new KeyFrame(50, new EulerAngle(Math.toRadians(-20), Math.toRadians(65), Math.toRadians(-30))));
		animation.put("phantom_empress/leftarm", new SequenceAnimation(new Sequence(leftArmKeys)));

		List<KeyFrame> forearmKeys = new ArrayList<KeyFrame>();
		forearmKeys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-60), 0, 0)));
		forearmKeys.add(new KeyFrame(10, new EulerAngle(Math.toRadians(-75), 0, 0)));
		forearmKeys.add(new KeyFrame(46, new EulerAngle(Math.toRadians(-75), 0, 0)));
		forearmKeys.add(new KeyFrame(50, new EulerAngle(Math.toRadians(-60), 0, 0)));
		animation.put("phantom_empress/rightforearm", new SequenceAnimation(new Sequence(forearmKeys)));
		animation.put("phantom_empress/leftforearm", new SequenceAnimation(new Sequence(forearmKeys)));
		
	}

	@Override
	public void entityParentConnection(Entity parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		if(animation.containsKey(part.getModelName())) {
			animation.get(part.getModelName()).entityParentConnection(parent, target, part, head, body);
		}
		
	}

	@Override
	public void partParentConnection(ArmorStand parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		if(animation.containsKey(part.getModelName())) {
			animation.get(part.getModelName()).partParentConnection(parent, target, part, head, body);
		}
		
	}

	@Override
	public Animation createAnimation() {
		
		return new AnimationCast();
		
	}

	@Override
	public boolean containsPartAnimation(Part part) {
		return animation.containsKey(part.getModelName());
	}

}
