package com.ticxo.demomodel.model.animation.walk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.util.EulerAngle;

import com.ticxo.modelapi.api.animation.Animation;
import com.ticxo.modelapi.api.animation.preset.KeyFrame;
import com.ticxo.modelapi.api.animation.preset.Sequence;
import com.ticxo.modelapi.api.animation.preset.SequenceAnimation;
import com.ticxo.modelapi.api.modeling.ModelBase;
import com.ticxo.modelapi.api.modeling.Part;

public class AnimationWalk implements Animation{

	private ModelBase model;
	private Map<String, Animation> animation = new HashMap<String, Animation>();
	
	public AnimationWalk(ModelBase model) {
		
		this.model = model;
		for(String partId : model.getParts().keySet())
			animation.put(partId, createSequence(partId));
		
	}
	
	@Override
	public void entityParentConnection(Entity parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		animation.get(part.getModelName()).entityParentConnection(parent, target, part, head, body);
		
	}

	@Override
	public void partParentConnection(ArmorStand parent, ArmorStand target, Part part, EulerAngle head, EulerAngle body) {
		
		animation.get(part.getModelName()).partParentConnection(parent, target, part, head, body);
		
	}

	@Override
	public Animation createAnimation() {
		return new AnimationWalk(model);
	}

	@Override
	public boolean containsPartAnimation(Part part) {
		return animation.containsKey(part.getModelName());
	}
	
	private Animation createSequence(String partId){
		
		List<KeyFrame> keys = new ArrayList<KeyFrame>();
		
		switch(partId.split("/")[1]) {
		case "head":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(5), 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(5), 0, 0)));
			return new SequenceAnimation(new Sequence(keys), true);
		case "body":
		case "hip":
		case "waist":
			return new VelocityJoint(3, 10, new EulerAngle(0, 0, 0));
		case "upperdress":
		case "middledress":
		case "lowerdress":
		case "crowndress":
			return new VelocityJoint(3, 10, new EulerAngle(0, 0, 0));
		case "rightarm":
			return new VelocityJoint(3, 45, new EulerAngle(0, 0, Math.toRadians(20)));
		case "leftarm":
			return new VelocityJoint(3, 45, new EulerAngle(0, 0, Math.toRadians(-20)));
		case "rightthigh":
		case "rightforeleg":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(10), 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(10), 0, 0)));
			return new SequenceAnimation(new Sequence(keys));
		case "leftthigh":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(-35), 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(-35), 0, 0)));
			return new SequenceAnimation(new Sequence(keys));
		case "leftforeleg":
			keys.add(new KeyFrame(0, new EulerAngle(Math.toRadians(80), 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(Math.toRadians(80), 0, 0)));
			return new SequenceAnimation(new Sequence(keys));
		default:
			keys.add(new KeyFrame(0, new EulerAngle(0, 0, 0)));
			keys.add(new KeyFrame(20, new EulerAngle(0, 0, 0)));
			return new SequenceAnimation(new Sequence(keys));
		}
				
	}

}
