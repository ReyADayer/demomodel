package com.ticxo.demomodel;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.ticxo.demomodel.model.PhantomEmpress;
import com.ticxo.modelapi.ModelAPI;

public class Main extends JavaPlugin{

	public static JavaPlugin plugin;
	
	private static ConsoleCommandSender cs;
	
	public void onEnable() {
		
		plugin = this;
		
		cs = Bukkit.getServer().getConsoleSender();
		
		cs.sendMessage("[Demo Model] Releasing the monster...");
		
		registerEntityModel();
		
	}

	public void onDisable() {
		
		cs.sendMessage("[Demo Model] Retrieving the monster...");
		
	}
	
	public void registerEntityModel() {
		
		ModelAPI.registerPlugin(plugin, "tex.zip");
		ModelAPI.registerModel(new PhantomEmpress());
		
	}
	
}
